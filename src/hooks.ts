
import type { Handle } from "@sveltejs/kit";
import { pdfViewerHook } from '$lib';
import { base } from "$app/paths";

export const handle: Handle = pdfViewerHook({
  base,
});