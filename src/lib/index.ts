import * as fs from "fs/promises";
import * as path from 'path';

import type { Handle } from "@sveltejs/kit";

export interface PDFViewerHookParameters {
  base: string,
  prefix?: string,
  ssr?: boolean,
  build?: string,
}

export function pdfViewerHook(params: PDFViewerHookParameters): Handle {
  const prefix = params?.prefix ? params.prefix : "";
  const ssr = (params && Object.keys(params).includes("ssr")) ? params.ssr : true;

  const pdfBuildPath = path.resolve(params?.build || "./node_modules/pdfjs-viewer-build/viewer/");
  const indexPath = path.join(pdfBuildPath, "index.html");

  const base = path.posix.join("/", params.base, prefix);

  const indexSourcePromise = fs.readFile(indexPath, { encoding: "utf-8" });

  return async ({ event, resolve }) => {
    const pathname = event.url.pathname;
    // console.debug({ pathname, base })

    if (pathname.startsWith(base) && event.routeId !== null) {
      let out = await indexSourcePromise;

      return await resolve(event, { ssr, transformPage: ({ html }) => {
        out = out.replace("</head>", html.match(/<head>(.*?<\/head>)/s)?.[1] || "</head>");
        out = out.replace("</body>", html.match(/<body>(.*?<\/body>)/s)?.[1] || "</body>");
        return out;
      }})
    } else {
      return await resolve(event);
    }
  }
}